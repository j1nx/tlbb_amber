import sys,urllib,time
import xbmc,xbmcgui
import zipfile
from xbmcjson import XBMC
import xml.etree.ElementTree as ET

def all(_in, _out, dp=None):
    if dp:
        return allWithProgress(_in, _out, dp)
    
    return allNoProgress(_in, _out)

def allNoProgress(_in, _out):
    try:
        zin = zipfile.ZipFile(_in, 'r')
        zin.extractall(_out)
    except Exception, e:
        print str(e)
        return False
    
    return True

def allWithProgress(_in, _out, dp):
    zin    = zipfile.ZipFile(_in,  'r')
    nFiles = float(len(zin.infolist()))
    count  = 0
    
    try:
        for item in zin.infolist():
            count += 1
            update = count / nFiles * 100
            dp.update(int(update))
            zin.extract(item, _out)
    except Exception, e:
        print str(e)
        return False
    
    return True

def download(url, dest, dp = None):
    try:
        if not dp:
            dp = xbmcgui.DialogProgress()
            dp.create("Status...","Checking Installation",' ', ' ')
        dp.update(0)
        urllib.urlretrieve(url,dest,lambda nb, bs, fs, url=url: _pbhook(nb,bs,fs,url,dp))
    except Exception as e:
        print str(e)
        xbmc.executebuiltin( "Dialog.Close(busydialog)" )
        xbmcgui.Dialog().ok('Error','Download failed')
        
def _pbhook(numblocks, blocksize, filesize, url, dp):
    try:
        percent = min((numblocks*blocksize*100)/filesize, 100)
        dp.update(percent)
    except:
        percent = 100
        dp.update(percent)
        
    if dp.iscanceled(): 
        raise Exception("Canceled")
        dp.close()

skinUrlDict    = {"Amber"       :"http://127.0.0.1:8000/skin.amber.zip",
                  "Confluence"  :"http://cloud.thelittleblackbox.co.uk/repo/zips/skin.confluence-TLBB.zip",
                  "Metropolis " :"http://cloud.thelittleblackbox.co.uk/repo/zips/skin.metropolis-TLBB.zip",
                  "MetroTV"     :"http://cloud.thelittleblackbox.co.uk/repo/zips/skin.metroTV-TLBB.zip",
                  "TLBB"        :"http://cloud.thelittleblackbox.co.uk/repo/zips/skin.tlbb.zip"}

packagesFolder = xbmc.translatePath('special://home/addons/packages')
addonsFolder   = xbmc.translatePath('special://home/addons')
skinToSet      = sys.argv[1]
skinUrl        = skinUrlDict.get(skinToSet)
skinZip        = os.path.basename(skinUrl)
skinZipPath    = os.path.join(packagesFolder,skinZip)

if skinToSet != 'Amber':
    xbmc.executebuiltin( "ActivateWindow(busydialog)" )
    dp = xbmcgui.DialogProgress()
    dp.create('Download Progress:', 'Downloading skin...', '', 'Please Wait')
    
    if skinToSet != 'TLBB':
        #Download selected skin
        download(skinUrl, skinZipPath, dp)
        
        #extract downloaded skin
        all(skinZipPath, addonsFolder, dp)
        dp.close()
        
        skinFolder     = skinZip.rsplit('.',1)[-2]
        skinFolderPath = os.path.join(addonsFolder,skinFolder)
        skinAddon_xml  = os.path.join(skinFolderPath,'addon.xml')
        tree           = ET.parse(skinAddon_xml)
        root           = tree.getroot()
        skinId         = root.attrib.get('id')
        xbmc.executebuiltin( "Dialog.Close(busydialog)" )
    else:
        skinId         = 'skin.tlbb'
        
        
    #Set skin
    jsonxbmc       = XBMC("http://localhost:80/jsonrpc")
    jsonxbmc.Settings.SetSettingValue({"setting":"lookandfeel.skin","value":skinId})
