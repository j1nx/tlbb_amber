import sys
from xbmcjson import XBMC

languageToSet = sys.argv[1]
xbmc = XBMC("http://localhost:80/jsonrpc")
xbmc.Settings.SetSettingValue({"setting":"locale.subtitlelanguage","value":languageToSet})
xbmc.Settings.SetSettingValue({"setting":"subtitles.languages","value":[languageToSet,'English']})
