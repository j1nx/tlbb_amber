import xbmc
import os,sys


skinFolder  = xbmc.translatePath('special://home/addons/skin.amber-TLBB')

def renameFile(filesList, fromMode, toMode):
         
    for file in filesList:
        os.rename(file, file + fromMode)
        os.rename(file + toMode, file)


if sys.argv[1]   == 'ORG':
    fromMode      = '.tlbb'
    toMode        = '.ambr'

elif sys.argv[1] == 'TLBB':
    fromMode      = '.ambr'
    toMode        = '.tlbb'


inDir_1080i       = os.path.join(skinFolder, '1080i')
inDir_colors      = os.path.join(skinFolder, 'colors')
inDir_backgrounds = os.path.join(skinFolder, 'backgrounds')

filesList         = [os.path.join(inDir_1080i, 'DialogButtonMenu.xml'),
                     os.path.join(inDir_1080i, 'Home.xml'), 
                     os.path.join(inDir_1080i, 'Includes.xml'), 
                     os.path.join(inDir_1080i, 'Includes_Horizontal_Home.xml'), 
                     os.path.join(inDir_1080i, 'Includes_Vertical_Home.xml'), 
                     os.path.join(inDir_1080i, 'Settings.xml'),
                     os.path.join(inDir_1080i, 'Startup.xml'),
                     os.path.join(inDir_colors, 'defaults.xml'),
                     os.path.join(inDir_backgrounds, 'Music.jpg'), 
                     os.path.join(inDir_backgrounds, 'Movies.jpg'), 
                     os.path.join(inDir_backgrounds, 'Settings.jpg'),
                     os.path.join(inDir_backgrounds, os.path.join('default', 'default.jpg'))]

renameFile(filesList, fromMode, toMode)

xbmc.executebuiltin('ReloadSkin()')
    